class Infuru:
    settings = {
        'network': {
            'websocket': 'wss://%s.infura.io/ws/'
        },
        'allowed_networks': [
            'ropsten'
        ],
        'available_networks': [
            'mainnet',
            'ropsten',
            'rinkeby',
            'kovan',
            'goerli',
        ],
        'subscription_ids': {
            1: 'newPendingTransactions',
            2: 'newHeads',
        },
        'labels': {
            'newPendingTransactions': 'New Transactions',
            'newHeads': 'New Blocks'
        }
    }
