import json


class EventsBaseModel(object):
    def __init__(self, conf, network):
        self.conf = conf
        self.network = network

    def new_subscription(self, _id, _method, _params):
        args = {
            "id": _id,
            "method": _method,
            "params": [', '.join(i for i in _params)]
        }
        return args

    def prepare_subscription_id(self, _subscription_type):
        subscription_dict = {}
        for k, v in self.conf.Infuru.settings['subscription_ids'].items():
            if v not in subscription_dict:
                subscription_dict[v] = k
        _subscription_id = subscription_dict.get(_subscription_type, 1)
        return _subscription_id

    def get_subscription(self, _response):
        subscription = {}
        if _response:
            _response = json.loads(_response)
            if _response.get('id', None):
                subscription['id'] = _response['id']
                subscription['hash'] = _response['result']
                subscription['type'] = self.conf.Infuru.settings['subscription_ids'].get(int(subscription['id']), None)

        return subscription

    def parse_result(self, _response, _subscription_details):
        _log = ''
        if _subscription_details:
            if _response:
                _response = json.loads(_response)

                _messages = {
                    'newPendingTransactions': 'New transaction with hash %s',
                    'newHeads': 'New block created with number %s',
                }
                if _response.get('params', {}).get('result', None):
                    if _subscription_details['type'] == 'newPendingTransactions':
                        _hash = _response.get('params', {}).get('result', None)
                        if _hash:
                            _log = _messages[_subscription_details['type']] % _hash
                    elif _subscription_details['type'] == 'newHeads':
                        _number = _response.get('params', {}).get('result', {}).get('number', None)
                        if _number:
                            _log = _messages[_subscription_details['type']] % _number
                    else:
                        _log = _response
        return _log
