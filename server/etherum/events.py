import os
import sys
import asyncio
import websockets
import json
import importlib
import argparse
# add path
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '../../server')))
from etherum.models import EventsBaseModel
# add and parse arguments
parser = argparse.ArgumentParser()
parser.add_argument('--network', type=str, help='Infuru network name.')
parser.add_argument('--settings', type=str, help='Configuration file.')
parser.add_argument('--subscription', type=str, help='Subscription type.')
args = parser.parse_args()

_settings = args.settings
if not _settings:
    raise Exception('Settings parameter is required.')
_network = args.network
if not _network:
    raise Exception('Network parameter is required.')
_subscription = args.subscription
if not _subscription:
    raise Exception('Subscription parameter is required.')

# import config
try:
    conf = importlib.import_module('config.' + _settings)
except ImportError:
    raise Exception('No module %s name' % _settings)
# check allowed networks
if _network not in conf.Infuru.settings['allowed_networks']:
    raise Exception('Network specified is not allowed. Allowed networks -- %s --' % ', '.join(i for i in conf.Infuru.settings['allowed_networks']))  # noqa

allowed_subscriptions = [v for k, v in conf.Infuru.settings['subscription_ids'].items()]
if _subscription not in allowed_subscriptions:
    raise Exception('Subscription specified is not allowed. Allowed subscriptions -- %s --' % ', '.join(i for i in allowed_subscriptions))  # noqa

model = EventsBaseModel(
    conf=conf,
    network=_network
)


async def main():
    network_uri = conf.Infuru.settings['network']['websocket'] % _network

    while True:
        async with websockets.connect(network_uri) as websocket:
            print(' [*] Waiting for messages. To exit press CTRL + C')
            # subscribe
            custom_subscription_id = conf.Infuru.settings['subscription_ids']
            _event_subscribe = model.new_subscription(
                _id=model.prepare_subscription_id(_subscription_type=_subscription),
                _method='eth_subscribe',
                _params=[_subscription]
            )

            # get subscription details
            await websocket.send(json.dumps(_event_subscribe))
            subscription_response = await websocket.recv()
            subscription_details = model.get_subscription(_response=subscription_response)
            if subscription_details:
                _label = conf.Infuru.settings['labels'][subscription_details['type']]
                print('Subscribed to %s' % _label)
                print('listening...')
            # listen for messages
            while True:
                try:
                    response = await websocket.recv()
                    result = model.parse_result(_response=response, _subscription_details=subscription_details)
                    if result:
                        print(result)
                except KeyboardInterrupt:
                    break


if __name__ == '__main__':
    asyncio.get_event_loop().run_until_complete(main())
    asyncio.get_event_loop().run_forever()

